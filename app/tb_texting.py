__version__ = '0.1'

import re

TYPE_PARA = 1
TYPE_HEADER = 2
TYPE_LIST = 3
TYPE_CODE = 4
TYPE_QUOTE = 5
TYPE_TABLE = 6
TYPE_IMAGE = 7


class MDBlocks:

    def __init__(self):
        self.blocks = []
        self.types = []

    def __iter__(self):
        self.shitty_idx = 0
        return self

    def __next__(self):
        if self.shitty_idx < len(self.blocks):
            rz = (self.types[self.shitty_idx], self.blocks[self.shitty_idx])
            self.shitty_idx += 1
            return rz
        else:
            raise StopIteration

    def block_new(self, block_type: int, block: str):
        self.blocks.append(block)
        self.types.append(block_type)

    def block_join(self, block_type: int, block: str):
        if len(self.types) > 0 and self.types[-1] == block_type:
            self.blocks[-1] = self.blocks[-1] + '\n' + block
        else:
            self.block_new(block_type, block)


def split(rawtext: str) -> MDBlocks:
    md_blocks = MDBlocks()

    right_after_empty_line = True
    inside_code_block = False
    for a in rawtext.split('\n'):
        unstripped = a
        a = a.strip()

        if a.startswith('```'):
            inside_code_block = not inside_code_block
            if a[3:]:
                md_blocks.block_new(TYPE_CODE if inside_code_block else TYPE_PARA, a[3:])

        elif inside_code_block:
            md_blocks.block_join(TYPE_CODE, unstripped)

        elif a.startswith('#'):
            md_blocks.block_new(TYPE_HEADER, a)

        elif a.startswith('|'):
            md_blocks.block_join(TYPE_TABLE, a)

        elif a.startswith('* '):
            md_blocks.block_join(TYPE_LIST, unstripped)

        elif a.startswith('!['):
            md_blocks.block_new(TYPE_IMAGE, a)

        elif not a:  # Shitty python empty string check
            right_after_empty_line = True
            continue

        elif right_after_empty_line:
            md_blocks.block_new(TYPE_PARA, a)

        else:
            md_blocks.block_join(TYPE_PARA, a)

        right_after_empty_line = False

    return md_blocks


def to_bbcode(s):
    links = {}
    codes = []

    def gather_link(m):
        links[m.group(1)] = m.group(2)
        return ""

    def replace_link(m):
        if m.group(2) in links or m.group(1) in links:
            return "[color=#e70][ref=%s]%s[/ref][/color]" % (links[m.group(2) or m.group(1)], m.group(1))
        else:
            return m.group(1) + " [sup]broken link[/sup]"

    def gather_code(m):
        codes.append(m.group(3))
        return "[code=%d]" % len(codes)

    def replace_code(m):
        return "%s" % codes[int(m.group(1)) - 1]

    def translate(p="%s", g=1):
        def inline(m):
            s = m.group(g)
            s = re.sub(r"(`+)(\s*)(.*?)\2\1", gather_code, s)
            s = re.sub(r"\[(.*?)\]\[(.*?)\]", replace_link, s)
            s = re.sub(r"\[(.*?)\]\((.*?)\)", "[ref=\\2]\\1[/ref]", s)
            s = re.sub(r"<(https?:\S+)>", "[ref=\\1]\\1[/ref]", s)
            s = re.sub(r"\B([*_]{2})\b(.+?)\1\B", "[b]\\2[/b]", s)
            s = re.sub(r"\B([*_])\b(.+?)\1\B", "[i]\\2[/i]", s)
            return p % s

        return inline

    s = re.sub(r"(?m)^\[(.*?)]:\s*(\S+).*$", gather_link, s)
    #    s = re.sub(r"(?m)^    (.*)$", "~[code]\\1[/code]", s)
    s = re.sub(r"(?m)^(\S.*)\n=+\s*$", translate("~[size=24][b]%s[/b][/size]"), s)
    s = re.sub(r"(?m)^(\S.*)\n-+\s*$", translate("~[size=22][b]%s[/b][/size]"), s)
    s = re.sub(r"(?m)^> (.*)$", translate("~[quote]%s[/quote]"), s)
    #    s = re.sub(r"(?m)^[-+*]\s+(.*)$", translate("~[list]\n[*]%s\n[/list]"), s)
    #    s = re.sub(r"(?m)^\d+\.\s+(.*)$", translate("~[list=1]\n[*]%s\n[/list]"), s)
    s = re.sub(r"(?m)^((?!~).*)$", translate(), s)
    s = re.sub(r"(?m)^~\[", "[", s)
    s = re.sub(r"\[/code]\n\[code(=.*?)?]", "\n", s)
    s = re.sub(r"\[/quote]\n\[quote]", "\n", s)
    s = re.sub(r"\[/list]\n\[list(=1)?]\n", "", s)
    s = re.sub(r"(?m)\[code=(\d+)]", replace_code, s)

    return s


def wrap_refs(text: str):
    return text.replace('[ref=', '[color=#e70][b][ref=').replace('[/ref]', '[/ref][/b][/color]')


def para_preview(para_text: str):
    return para_text.replace('\n', ' ')[:15].strip()


def url_from_md_href(para_text: str):
    s = para_text.find('(')
    e = para_text.find(')')
    return para_text[s + 1: e - s + 3]
