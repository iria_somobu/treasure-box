import json
import math
import os
import time
import uuid

from database import DB


def system_uri(uri: str):
    os.system(f"start \"{uri}\"")
    os.system(f"open \"{uri}\"")
    os.system(f"xdg-open \"{uri}\"")


def string2tags(string: str):
    tags = [x.strip() for x in string.split(',')]
    tags = [x for x in tags if x]
    tags.sort()
    tags = list(set(tags))
    return tags


class ArticleMeta:

    def __init__(self):
        self.uuid = ''
        self.title = ''

        self.origin_time = 0
        self.origin_url = ''

        self.create_time = 0
        self.created_by = ''

        self.edit_time = 0
        self.edit_by = ''

        self.tags = []


class ArticlePara:

    def __init__(self):
        self.text = ''
        self.edit_time = 0
        self.edit_by = ''
        self.p_type = 0
        self.order = 0


class Article:

    def __init__(self):
        self.meta = ArticleMeta()
        self.paragraphs = []


class FeedEvent:
    EVENT_ADD_ARTICLE_META = 10
    EVENT_EDIT_ARTICLE_META = 11
    EVENT_EDIT_PARAGRAPH = 13
    EVENT_ADD_ARTICLE_COMMENT = 20
    EVENT_ADD_PARAGRAPH_COMMENT = 25

    def __init__(self):
        self.actor_uuid = ''
        self.actor = ''
        self.action_type = 0
        self.object_name = ''
        self.object_uuid = ''
        self.location_name = ''
        self.location_uuid = ''
        self.time_days = 0

    def __lt__(self, other):
        return (other.time_days, self.action_type, self.actor) \
               < (self.time_days, other.action_type, other.actor)

    def __eq__(self, other):
        return (self.time_days, self.action_type, self.actor) \
               == (other.time_days, other.action_type, other.actor)


class Tag:

    def __init__(self):
        self.name = ''
        self.parent = ''
        self.descr = ''

    def __lt__(self, other):
        return self.name.lower() < other.name.lower()

    def __eq__(self, other):
        return self.name.lower() == other.name.lower()


class Comment:
    def __init__(self):
        self.uuid = ''
        self.edit_time = ''
        self.author = ''
        self.author_name = ''
        self.comment = ''


class QueriedArticleMeta:

    def __init__(self):
        self.uuid = ''
        self.title = ''
        self.edit_time = 0
        self.edit_by = ''
        self.edit_uname = ''


db = DB()


def create():
    db.open()


def destroy():
    db.close()


def time_millis() -> int:
    return round(time.time() * 1000)


def can_db() -> bool:
    return not db.is_locked()


def has_account() -> bool:
    return db.get_meta('user') is not None


def my_uuid() -> str:
    return db.get_meta('user_id', None)


def new_account(name):
    _uuid = str(uuid.uuid4())
    db.set_meta('user', name)
    db.set_meta('user_id', _uuid)
    db.insert_account(_uuid, name)


def post(article: Article):
    post_meta(article.meta)

    for i in range(len(article.paragraphs)):
        para = article.paragraphs[i]
        db.insert_text(para.edit_time, article.meta.uuid, para.edit_by, i, para.p_type, para.text)

    for tag in article.meta.tags:
        db.insert_tag(tag)
        db.link_article_tag(article.meta.uuid, tag, time_millis(), my_uuid())


def post_meta(article_meta: ArticleMeta):
    db.insert_meta(article_meta.uuid, article_meta.edit_time, article_meta.edit_by,
                   article_meta.title, article_meta.origin_url)


def post_para(article_uuid: str, para: ArticlePara):
    db.insert_text(para.edit_time, article_uuid, para.edit_by, para.order, para.p_type, para.text)


def get_feed():
    rz = []

    for meta in db.get_meta_changes():
        fe = FeedEvent()

        if meta[2] == meta[5]:
            fe.action_type = FeedEvent.EVENT_ADD_ARTICLE_META
        else:
            fe.action_type = FeedEvent.EVENT_EDIT_ARTICLE_META

        fe.object_uuid = meta[0]
        fe.object_name = meta[1]
        fe.time_days = math.floor(meta[2] / (24 * 60 * 60 * 1000))
        fe.actor_uuid = meta[3]
        fe.actor = meta[4]
        rz.append(fe)

    pc = []
    for para in db.get_para_changes():

        if (para[0], para[2], para[4]) in pc:
            continue

        fe = FeedEvent()
        fe.action_type = FeedEvent.EVENT_EDIT_PARAGRAPH

        fe.actor_uuid = para[0]
        fe.actor = para[1]
        fe.object_uuid = para[2]
        fe.object_name = para[3]
        fe.location_uuid = para[4]
        fe.location_name = para[5]
        fe.time_days = math.floor(para[6] / (24 * 60 * 60 * 1000))

        rz.append(fe)
        pc.append((para[0], para[2], para[4]))

    for comm in db.get_comment_changes():
        fe = FeedEvent()

        if comm[2] is None:
            fe.action_type = FeedEvent.EVENT_ADD_ARTICLE_COMMENT
        else:
            fe.action_type = FeedEvent.EVENT_ADD_PARAGRAPH_COMMENT

        fe.actor_uuid = comm[0]
        fe.actor = comm[1]
        fe.object_uuid = comm[2]
        fe.object_name = comm[3]
        fe.location_uuid = comm[4]
        fe.location_name = comm[5]
        fe.time_days = math.floor(comm[6] / (24 * 60 * 60 * 1000))

        rz.append(fe)

    return sorted(rz)


def get_article_by_id(art_id: str) -> Article:
    art = Article()
    art.meta = get_article_meta(art_id)

    for para in db.get_latest_text(art_id):
        ap = ArticlePara()
        ap.order = para[3]
        ap.p_type = para[4]
        ap.text = para[5]
        art.paragraphs.append(ap)

    art.meta.tags = get_article_tags(art_id)

    return art


def get_article_meta(art_id: str) -> ArticleMeta:
    art_meta = ArticleMeta()
    meta = db.get_latest_meta(art_id)

    art_meta.uuid = meta[0]
    art_meta.edit_time = meta[1]
    art_meta.edit_by = meta[2]
    art_meta.title = meta[3]
    art_meta.origin_url = meta[4]
    art_meta.origin_time = meta[5]
    art_meta.create_time = meta[6]
    art_meta.created_by = meta[7]

    return art_meta


def get_article_tags(art_id: str):
    tags = []

    for tag in db.get_article_tags(art_id):
        tags.append(tag[0])

    return tags


def alter_article_tags(art_id: str, base_list, changed_list):
    added = set(changed_list) - set(base_list)
    for tag in added:
        db.insert_tag(tag)
        db.link_article_tag(art_id, tag, time_millis(), my_uuid())

    removed = set(base_list) - set(changed_list)
    for tag in removed:
        db.link_article_tag(art_id, tag, time_millis(), my_uuid(), True)


def uname(user_id):
    u = db.get_username(user_id)
    if u:
        return u[0]
    else:
        return 'Unknown user'


def list_tags():
    rz = []

    for tag in db.list_tags():
        t = Tag()
        t.name = tag[0]
        t.parent = tag[1]
        t.descr = tag[2]
        rz.append(t)

    rz.sort()
    return rz


def list_articles():
    rz = []

    for article in db.get_all_articles():
        a = ArticleMeta()
        a.uuid = article[0]
        a.edit_time = article[1]
        a.edit_by = article[2]
        a.title = article[3]
        a.origin_url = article[4]
        a.origin_time = article[5]

        rz.append(a)

    return rz


def rate_article(article: str, rating: int):
    db.insert_reaction(article, my_uuid(), rating, time_millis())


def get_my_reaction(article: str) -> int:
    reaction = db.get_reaction(article, my_uuid())

    if reaction is None:
        return -2
    else:
        return reaction[0]


def post_comment(article: str, para, comment: str) -> Comment:
    c = Comment()
    c.uuid = str(uuid.uuid4())
    c.edit_time = time_millis()
    c.author_name = 'you'
    c.author = my_uuid()
    c.comment = comment

    db.insert_comment(c.uuid, c.edit_time, c.author, article, para, comment)

    return c


def get_comments(article: str, para):
    rz = []

    for comm in db.get_comments(article, para):
        c = Comment()
        c.uuid = comm[0]
        c.edit_time = comm[1]
        c.author = comm[2]
        c.author_name = comm[3]
        c.comment = comm[4]
        rz.append(c)

    return rz


def get_para_text(article: str, para: int) -> ArticlePara:
    data = db.get_para_text(article, para)

    rz = ArticlePara()
    rz.text = data[0]
    rz.edit_time = data[1]
    rz.edit_by = data[2]
    rz.p_type = data[3]
    rz.order = data[4]

    return rz


def query_by_tag(tag: str):
    rz = []

    for row in db.get_articles_by_tag(tag):
        a = QueriedArticleMeta()
        a.uuid = row[0]
        a.title = row[1]
        # a.edit_time = row[2]
        # a.edit_by = row[3]
        # a.edit_uname = row[4]
        rz.append(a)

    return rz


def query_by_substring(substr: str):
    rz = []

    for row in db.get_articles_by_substr(substr):
        a = QueriedArticleMeta()
        a.uuid = row[0]
        a.title = row[1]
        a.edit_time = row[2]
        # a.edit_by = row[3]
        # a.edit_uname = row[4]
        rz.append(a)

    return rz


def export_json_file(filename: str, time: int):
    awawo = {
        'accounts': db.raw_query_accounts(),
        'meta': db.raw_query_meta(),
        'para': db.raw_query_text(),
        'reactions': db.raw_query_reactions(),
        'comments': db.raw_query_comments(),
        'tags': db.raw_query_tags(),
        'article_tags': db.raw_query_art_tags()
    }

    with open(filename, 'w') as f:
        f.write(json.dumps(awawo))


def import_json_file(filename: str):
    with open(filename, 'r') as file:
        data = file.read()

    awawo = json.loads(data)
    db.raw_insert_accounts(awawo['accounts'])
    db.raw_insert_meta(awawo['meta'])
    db.raw_insert_text(awawo['para'])
    db.raw_insert_reactions(awawo['reactions'])
    db.raw_insert_comments(awawo['comments'])
    db.raw_insert_tags(awawo['tags'])
    db.raw_insert_art_tags(awawo['article_tags'])
