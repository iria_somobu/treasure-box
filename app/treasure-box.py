#!/usr/bin/python3

import datetime
import time
import uuid
from urllib.parse import urlparse

import kivy
from kivy import Config
from kivy.app import App
from kivy.core.text import LabelBase
from kivy.core.window import Window
from kivy.factory import Factory
from kivy.lang.builder import Builder
from kivy.metrics import dp
from kivy.properties import BooleanProperty, ObjectProperty
from kivy.uix.behaviors.button import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen, WipeTransition, NoTransition

import api
import tb_texting

time_format_date = '%d %b %Y'
hover_opacity = [0.0, 1.0]
bgcolor = (0.975, 0.975, 0.975, 1)


def node_search_name(source, _name):
    for node in source.walk():
        try:
            if node.name == _name:
                return node
        except AttributeError:
            pass
    return None


class TreasureBox(App):
    header = None
    sm = None

    uri_back_stack = []
    edited_article = api.Article()

    URI_FEED = 'tb://feed'

    q_small = BooleanProperty(False)
    q_medium = BooleanProperty(False)

    def build(self):
        calc_responsibility(None, None, None)

        self.title = 'Treasure Box'
        self.root = Builder.load_file("kv/app.kv")
        self.header = self.root.ids.header
        self.sm = self.root.ids.screen_mgr
        self.sm.transition = NoTransition()

        if api.has_account():
            self.root.ids.header.locked = False
            self.route_uri(TreasureBox.URI_FEED)

        self.sm.transition = WipeTransition()

        self.recreate_article()

        return self.root

    def recreate_article(self):
        self.edited_article = api.Article()
        self.edited_article.meta.uuid = str(uuid.uuid4())
        self.edited_article.meta.created_by = api.my_uuid()
        self.edited_article.meta.create_time = round(time.time() * 1000)
        self.edited_article.meta.edit_by = api.my_uuid()
        self.edited_article.meta.edit_time = round(time.time() * 1000)

    def on_login_press(self):
        username = node_search_name(self.root, 'login_value').text.strip()

        if not username:
            content = Builder.load_file('kv/popup_label_1btn.kv')
            popup = Popup(title='Пустой юзернейм', title_color=[0, 0, 0, 1], content=content,
                          auto_dismiss=False, size_hint=(None, None), size=(dp(400), dp(400)))
            node_search_name(content, 'label').text = "Жить без имени неприемлемо"
            node_search_name(content, 'confirm').bind(on_press=popup.dismiss)
            popup.open()
        elif not api.can_db():
            self.popup_cannot_db()
        else:
            api.new_account(username)
            self.recreate_article()
            self.root.ids.header.locked = False
            self.route_uri(TreasureBox.URI_FEED)

    def show_previous_route(self):
        if len(self.uri_back_stack) < 2:
            self.route_uri('tb://feed')
        elif self.can_switch_screen():
            self.uri_back_stack.pop()  # Drop current screen
            self.route_uri(self.uri_back_stack.pop())  # Go to previous screen

    def post_edited_article(self):
        # Force-update editor name and edit time
        self.edited_article.meta.edit_by = api.my_uuid()
        self.edited_article.meta.edit_time = round(time.time() * 1000)

        if api.can_db():
            api.post(self.edited_article)

            self.recreate_article()
            self.route_uri(TreasureBox.URI_FEED)
        else:
            self.popup_cannot_db()

    def open_react_dialog_article_screen(self):
        node_search_name(self.root, 'view_article').view_react_dialog()

    def perform_search(self):
        self.route_uri(f'tb://filter/by_words/{node_search_name(self.root, "s_n").text}')

    def can_switch_screen(self):
        return node_search_name(self.sm, self.sm.current) is not None

    def route_uri(self, uri, force=False):
        if uri == self.get_current_screen_uri():
            return  # We're already on this screen

        # TODO (somobu): w/o this hack app crashes on multiple quick screen changes
        if not self.can_switch_screen():
            return

        if not force and node_search_name(self.sm, self.sm.current).has_unsaved_progress():
            self.popup_unsaved_progress(uri)
            return

        self.uri_back_stack.append(uri)

        ps = urlparse(uri)
        host = ps.hostname
        path = ps.path.split('/')

        if ps.scheme == 'tb':
            if host == 'feed':
                self.switch_to_screen('screen_feed', 'Лента', 'btn_h_feed').reload_feed()
            elif host == 'search':
                self.switch_to_screen('screen_search', 'Поиск', 'btn_h_search')
            elif host == 'settings':
                self.switch_to_screen('screen_settings', 'Настройки', 'btn_h_settings')
            elif host == 'edit':
                if path[2] == 'para':
                    self.popup_edit_para(path[1], ps.fragment)
                elif path[2] == 'tags':
                    self.popup_edit_tags(path[1])
                elif path[2] == 'meta':
                    self.popup_edit_meta(path[1])
                self.uri_back_stack.pop()
            elif host == 'editor':
                self.switch_to_screen('screen_editor', 'Редактор', 'btn_h_add').view(self.edited_article)
            elif host == 'editor_preview':
                self.switch_to_screen('screen_view_article').view_article(self.edited_article, True)
            elif host == 'article':
                art_id = path[1]
                art = api.get_article_by_id(art_id)
                if art is not None:
                    self.switch_to_screen('screen_view_article').view_article(art, False)
                else:
                    self.view_error('Статья не найдена', 'UUID: ' + art_id)
            elif host == 'comments':
                screen = self.switch_to_screen('screen_view_comments', 'Комментарии')
                screen.view_comments(path[1], path[2], ps.fragment)
            elif host == 'hierarchy':
                self.view_tags_hier()
            elif host == 'index':
                self.view_index()
            elif host == 'user':
                screen = self.switch_to_screen('screen_user', 'Грузим юзера')
                user_id = path[1]
                node_search_name(screen, 'user_info').text = f'Тут будет инфа о пользователе\nID: {user_id}'
            elif host == 'filter':
                if path[1] == 'by_tag':
                    self.view_filter_tag(path[2])
                elif path[1] == 'by_words':
                    self.view_filter_word(path[2])
                else:
                    self.view_error('Wrong URI dude', 'Article filter [' + path[1] + "] is not supported")
            elif host == 'sync':
                self.switch_to_screen('screen_sync', 'Синхронизация', 'btn_h_sync')
            else:
                self.view_error('Wrong URI dude', 'Путь [' + uri + "] не поддерживается")
        else:
            self.switch_to_screen('screen_ext_uri', 'Переход на внешний URI').set_uri(uri)

    def get_current_screen_uri(self):
        if len(self.uri_back_stack) < 1:
            return None
        else:
            return self.uri_back_stack[-1]

    def popup_cannot_db(self):
        content = Builder.load_file('kv/popup_label_1btn.kv')
        popup = Popup(title='Database is locked', title_color=[0, 0, 0, 1], content=content,
                      auto_dismiss=False, size_hint=(None, None), size=(dp(400), dp(400)))
        node_search_name(content, 'label').text = "Похоже, сторонняя прога использует нашу БД. \n" \
                                                  "Прибей прогу и пробуй еще раз"
        node_search_name(content, 'confirm').bind(on_press=popup.dismiss)
        popup.open()

    def popup_unsaved_progress(self, uri: str):
        content = Builder.load_file('kv/popup_label_2btns.kv')
        popup = Popup(title='Unsafe transition', title_color=[0, 0, 0, 1], content=content,
                      auto_dismiss=False, size_hint=(None, None), size=(dp(400), dp(400)))
        node_search_name(content, 'label').text = "Данные будут утеряны при переходе. Норм?"
        node_search_name(content, 'cancel').bind(on_press=popup.dismiss)

        def force_poop(ignored_shit):
            popup.dismiss()
            self.route_uri(uri, True)

        node_search_name(content, 'confirm').bind(on_press=force_poop)
        popup.open()

    def popup_edit_para(self, article, para):

        content = Builder.load_file('kv/popup_field_and_button.kv')

        popup = Popup(title='Редачим абзац #' + para,
                      title_color=[0, 0, 0, 1], content=content, auto_dismiss=True,
                      size_hint=(0.8, 0.9))

        src_para = api.get_para_text(article, para)

        def confirm(unused):
            src_para.edit_time = api.time_millis()
            src_para.edit_by = api.my_uuid()
            src_para.text = content.ids.text_field.text

            if api.can_db():
                api.post_para(article, src_para)
                self.route_uri(self.uri_back_stack.pop())
            else:
                app().popup_cannot_db()

            popup.dismiss()

        content.ids.confirm_btn.bind(on_press=confirm)
        content.ids.text_field.text = src_para.text

        popup.open()

    def popup_edit_tags(self, article):
        content = Builder.load_file('kv/popup_field_and_button.kv')

        popup = Popup(title='Редачим тэги', title_color=[0, 0, 0, 1], content=content, auto_dismiss=True,
                      size_hint=(0.8, 0.9))

        source_tags = api.get_article_tags(article)

        source_text = ""
        if len(source_tags) > 0:
            for tag in source_tags:
                source_text += tag + ", "
            source_text = source_text[:-2]

        def confirm(unused):
            if api.can_db():
                api.alter_article_tags(article, source_tags, api.string2tags(content.ids.text_field.text))
                self.route_uri(self.uri_back_stack.pop())
            else:
                app().popup_cannot_db()

            popup.dismiss()

        content.ids.confirm_btn.bind(on_press=confirm)
        content.ids.text_field.text = source_text

        popup.open()

    def popup_edit_meta(self, article):
        content = Builder.load_file('kv/popup_edit_metadata.kv')

        popup = Popup(title='Редачим метадату', title_color=[0, 0, 0, 1], content=content, auto_dismiss=True,
                      size_hint=(None, None), size=(dp(400), dp(300)))
        meta = api.get_article_meta(article)

        def confirm(unused):
            meta.edit_time = api.time_millis()
            meta.edit_by = api.my_uuid()
            meta.title = content.ids.input_header.text
            meta.origin_url = content.ids.input_url.text

            if api.can_db():
                api.post_meta(meta)
                self.route_uri(self.uri_back_stack.pop())
            else:
                app().popup_cannot_db()

            popup.dismiss()

        content.ids.confirm_btn.bind(on_press=confirm)
        content.ids.input_header.text = meta.title
        content.ids.input_url.text = meta.origin_url

        popup.open()

    def switch_to_screen(self, name: str, title=None, active_header_btn=None):
        next_screen_boy = Builder.load_file(f"kv/{name}.kv")

        if next_screen_boy is None:
            raise RuntimeError(f"Dude, I'm unable to load kv/{name}.kv!")

        if title is not None:
            self.set_title(title)

        self.set_active_header_btn(active_header_btn)

        current = node_search_name(self.root, self.sm.current)

        for node in self.sm.screens:
            if node != current:
                self.sm.remove_widget(node)

        self.sm.add_widget(next_screen_boy)

        self.sm.current = next_screen_boy.name

        return next_screen_boy

    def set_title(self, title: str):
        # node_search_name(self.root, 'header_title').text = title
        self.title = title + ' — Treasure Box'

    def set_active_header_btn(self, header_btn_name):
        for button in node_search_name(self.root, 'header_buttons').children:
            if hasattr(button, 'active'):  # yup this is bad all around
                button.set_active(button.name == header_btn_name)

    def view_error(self, title, message):
        self.switch_to_screen('screen_error', 'Грузим ошибку').view_error(title, message)

    def view_index(self):
        screen = self.switch_to_screen('screen_index', 'Оглавление', 'btn_h_list')

        t = 'Список статей:\n'
        for article in api.list_articles():
            t += tb_texting.wrap_refs(f'\n    [ref=tb://article/{article.uuid}]{article.title}[/ref]')

        node_search_name(screen, 'index_screen_lbl').text = t

    def view_tags_hier(self):
        screen = self.switch_to_screen('screen_hierarchy', 'Тэги', 'btn_h_segment')

        t = 'Список тэгов:'
        last_char = '\0'
        for tag in api.list_tags():
            if tag.name[0].lower() != last_char:
                t += '\n\n'
                last_char = tag.name[0].lower()
            else:
                t += ', '

            t += tb_texting.wrap_refs(f'[ref=tb://filter/by_tag/{tag.name}]{tag.name}[/ref]')

        node_search_name(screen, 'hier_tags_lbl').text = t

    def view_filter_tag(self, tag: str):
        screen = self.switch_to_screen('screen_search_rz', 'Результаты поиска')

        t = f'Результаты по тэгу "{tag}":\n'
        for article in api.query_by_tag(tag):
            t += tb_texting.wrap_refs(f'\n - [ref=tb://article/{article.uuid}]{article.title}[/ref]')

        node_search_name(screen, 'hier_tags_lbl').text = t

    def view_filter_word(self, tag: str):
        screen = self.switch_to_screen('screen_search_rz', 'Результаты поиска')

        t = f'Результаты по подстроке "{tag}"\n:'
        for article in api.query_by_substring(tag):
            t += tb_texting.wrap_refs(f'\n - [ref=tb://article/{article.uuid}]{article.title}[/ref]')

        node_search_name(screen, 'hier_tags_lbl').text = t

    def license(self) -> str:
        return """    treasure-box
    Copyright (C) 2021  Iria Somobu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""


def app() -> TreasureBox:
    return TreasureBox.get_running_app()


def is_droid() -> bool:
    return kivy.utils.platform == 'android'


def calc_responsibility(ignored, ignored_2, ignored_3):
    app().q_small = Window.width < dp(480)
    app().q_medium = Window.width < dp(920)


# UI helpers


class HoverBehavior(object):
    """Hover behavior.
    :Events:
        `on_enter`
            Fired when mouse enter the bbox of the widget.
        `on_leave`
            Fired when the mouse exit the widget
    """

    hovered = BooleanProperty(False)
    border_point = ObjectProperty(None)
    '''Contains the last relevant point received by the Hoverable. This can
    be used in `on_enter` or `on_leave` in order to know where was dispatched the event.
    '''

    def __init__(self, **kwargs):
        self.register_event_type('on_enter')
        self.register_event_type('on_leave')
        Window.bind(mouse_pos=self.on_mouse_pos)
        super(HoverBehavior, self).__init__(**kwargs)

    def on_mouse_pos(self, *args):
        if not self.get_root_window():
            return  # do proceed if I'm not displayed <=> If have no parent
        pos = args[1]
        # Next line to_widget allow to compensate for relative layout
        inside = self.collide_point(*self.to_widget(*pos))
        if self.hovered == inside:
            # We have already done what was needed
            return
        self.border_point = pos
        self.hovered = inside
        if inside:
            self.dispatch('on_enter')
        else:
            self.dispatch('on_leave')

    def on_enter(self):
        pass

    def on_leave(self):
        pass


class ImageButton(ButtonBehavior, HoverBehavior, Image):
    active = BooleanProperty(False)
    hovered = False
    locked = False

    def __init__(self, **kwargs):
        super(ImageButton, self).__init__(**kwargs)

    def on_enter(self, *args):
        self.hovered = True
        self.color = self.color_from_state()
        self.canvas.ask_update()

    def on_leave(self, *args):
        self.hovered = False
        self.color = self.color_from_state()
        self.canvas.ask_update()

    def color_from_state(self):
        if self.locked:
            return [1, 1, 1, 0.5]
        if self.active:
            return [1, 0.5, 0, 1]
        else:
            return bgcolor if not self.hovered else (1, 0.8, 0.7, 1)

    def set_active(self, active):
        self.active = active
        self.color = self.color_from_state()
        self.canvas.ask_update()

    def update_color(self):
        self.color = self.color_from_state()
        self.canvas.ask_update()


class ArticleEntry(HoverBehavior, GridLayout):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.hover_enabled = True
        self.article_uuid = "DAT_URI"
        self.article_para = ""

    def on_enter(self, *args):
        if self.hover_enabled:
            self.ids.act.canvas.opacity = hover_opacity[1]

    def on_leave(self, *args):
        if self.hover_enabled:
            self.ids.act.canvas.opacity = hover_opacity[0]


class HeaderFragment(BoxLayout):
    locked = BooleanProperty(False)

    def on_locked(self, instance, value):
        for node in instance.walk():
            try:
                node.locked = value
                node.update_color()
            except AttributeError:
                pass

    def route_back(self):
        if not self.locked:
            app().show_previous_route()

    def route_uri(self, uri):
        if not self.locked:
            app().route_uri(uri)


class ParaEditbtnItem(BoxLayout):
    pass


# Screens

class MScreen(Screen):

    def has_unsaved_progress(self) -> bool:
        return False


class AccountScreen(MScreen):
    pass


class EditorScreen(MScreen):

    def has_unsaved_progress(self):
        inputs = [
            node_search_name(self, 'art_title'),
            node_search_name(self, 'art_url'),
            node_search_name(self, 'art_tags'),
            node_search_name(self, 'art_raw')
        ]
        return any(inp.text for inp in inputs)

    def view(self, article: api.Article):
        node_search_name(self, 'art_title').text = article.meta.title
        node_search_name(self, 'art_url').text = article.meta.origin_url
        node_search_name(self, 'art_tags').text = self.tags_to_raw(article)
        node_search_name(self, 'art_raw').text = self.paras_to_raw(article)

    def raw_to_paras(self, raw):
        app().edited_article.paragraphs = list()

        for r in tb_texting.split(raw):
            pp = api.ArticlePara()
            pp.p_type, pp.text = r
            pp.edit_time = round(time.time() * 1000)
            pp.edit_by = api.my_uuid()
            app().edited_article.paragraphs.append(pp)

    def paras_to_raw(self, article: api.Article):
        rz = ''
        for para in article.paragraphs:
            rz += para.text
            rz += '\n\n'

        return rz

    def get_proper_tags(self):
        return api.string2tags(node_search_name(self, 'art_tags').text)

    def tags_to_raw(self, article: api.Article):
        rz = ""

        if len(article.meta.tags) > 0:
            for tag in article.meta.tags:
                rz += tag + ', '
            rz = rz[:-2]

        return rz

    def values_to_data(self):
        app().edited_article.meta.title = node_search_name(self, 'art_title').text
        app().edited_article.meta.origin_url = node_search_name(self, 'art_url').text
        app().edited_article.meta.tags = self.get_proper_tags()

        self.raw_to_paras(node_search_name(self, 'art_raw').text)

    def preview_click(self):
        self.values_to_data()

        if not app().edited_article.meta.title.strip():
            self.popup_no_title()
        elif len(app().edited_article.paragraphs) > 0:
            app().route_uri('tb://editor_preview', force=True)
        else:
            self.popup_no_content()

    def popup_no_title(self):
        content = Builder.load_file('kv/popup_label_1btn.kv')
        popup = Popup(title='Статья без заголовка', title_color=[0, 0, 0, 1], content=content,
                      auto_dismiss=False, size_hint=(None, None), size=(dp(400), dp(400)))
        node_search_name(content, 'label').text = "У статьи нет заголовка\nРазмещать ее так -- неприемлемо"
        node_search_name(content, 'confirm').bind(on_press=popup.dismiss)
        popup.open()

    def popup_no_content(self):
        content = Builder.load_file('kv/popup_label_2btns.kv')
        popup = Popup(title='Статья без текста', title_color=[0, 0, 0, 1], content=content,
                      auto_dismiss=False, size_hint=(None, None), size=(dp(400), dp(400)))
        node_search_name(content, 'label').text = "В статье нет текста.\nВсе равно разместить ее?"
        node_search_name(content, 'cancel').bind(on_press=popup.dismiss)

        def force_poop(ignored_shit):
            popup.dismiss()
            app().post_edited_article()

        node_search_name(content, 'confirm').bind(on_press=force_poop)
        popup.open()


class ErrorScreen(MScreen):

    def view_error(self, title, text):
        app().set_title(title)
        self.ids.err_msg.text = text


class ExtUriScreen(MScreen):
    uri = ''

    def set_uri(self, uri: str):
        self.uri = uri
        node_search_name(self, 'warning_text').text = f"""Переход по внешней ссылке
{uri}
Точно открыть URI во внешнем приложении?"""


class FeedScreen(MScreen):

    def reload_feed(self):
        fc = node_search_name(self, 'feed_content')
        fc.clear_widgets()

        curday = 0
        for item in api.get_feed():
            if item.time_days != curday:
                curday = item.time_days
                ts = datetime.datetime.fromtimestamp(curday * 24 * 60 * 60)
                self.add_divider(fc, ts.strftime(time_format_date))

            at = item.action_type
            fmt = ''
            oname = kivy.utils.escape_markup(item.object_name)
            if at == api.FeedEvent.EVENT_ADD_ARTICLE_META:
                fmt = f'    [ref=tb://user/{item.actor_uuid}]{item.actor}[/ref] ' \
                      f'добавил статью [ref=tb://article/{item.object_uuid}]{oname}[/ref]'
            elif at == api.FeedEvent.EVENT_EDIT_ARTICLE_META:
                fmt = f'    [ref=tb://user/{item.actor_uuid}]{item.actor}[/ref] ' \
                      f'отредактировал мету статьи [ref=tb://article/{item.object_uuid}]{oname}[/ref]'
            elif at == api.FeedEvent.EVENT_EDIT_PARAGRAPH:
                fmt = f'    [ref=tb://user/{item.actor_uuid}]{item.actor}[/ref] ' \
                      f'отредактировал абзац "{tb_texting.para_preview(oname)}" статьи ' \
                      f'[ref=tb://article/{item.location_uuid}]{item.location_name}[/ref]'
            elif at == api.FeedEvent.EVENT_ADD_ARTICLE_COMMENT:
                fmt = f'    [ref=tb://user/{item.actor_uuid}]{item.actor}[/ref] ' \
                      f'добавил [ref=tb://comments/{item.location_uuid}/meta]комментарий[/ref] ' \
                      f'"{tb_texting.para_preview(oname)}" к статье ' \
                      f'[ref=tb://article/{item.location_uuid}]{item.location_name}[/ref]'
            elif at == api.FeedEvent.EVENT_ADD_PARAGRAPH_COMMENT:
                fmt = f'    [ref=tb://user/{item.actor_uuid}]{item.actor}[/ref] ' \
                      f'добавил [ref=tb://comments/{item.location_uuid}/para#{item.object_uuid}]комментарий[/ref] ' \
                      f'"{tb_texting.para_preview(oname)}" к абзацу статьи ' \
                      f'[ref=tb://article/{item.location_uuid}]{item.location_name}[/ref]'

            self.add_item(fc, tb_texting.wrap_refs(fmt))

    def add_divider(self, root, text):
        view = Builder.load_file("kv/fragment_feed_divider.kv")
        view.text = text
        root.add_widget(view)

    def add_item(self, root, text):
        view = Builder.load_file("kv/fragment_feed_item.kv")
        view.text = text
        root.add_widget(view)


class IndexScreen(MScreen):
    pass


class HierarchyScreen(MScreen):
    pass


class SearchScreen(MScreen):
    pass


class SearchRzScreen(MScreen):
    pass


class SettingsScreen(MScreen):
    pass


class SyncScreen(MScreen):

    def do_export(self):
        api.export_json_file('exported.json', 0)
        app().route_uri('exported.json')

    def do_import(self):
        if api.can_db():
            api.import_json_file('to_import.json')
            app().route_uri('tb://feed')
        else:
            app().popup_cannot_db()


class UserScreen(MScreen):
    pass


class ViewArticleScreen(MScreen):
    current_article = ''

    def view_article(self, article: api.Article, editor_preview=False):
        self.current_article = article.meta.uuid

        app().set_title(article.meta.title)
        sc_cc = node_search_name(self, 'scroll_content')
        sc_cc.clear_widgets()

        if editor_preview:
            sc_cc.add_widget(ParaEditbtnItem())

        cv = self.get_changelog_view(article.meta)
        cv.hover_enabled = not editor_preview
        sc_cc.add_widget(cv)

        tv = self.get_tags_view(article.meta)
        tv.hover_enabled = not editor_preview
        sc_cc.add_widget(tv)

        for para in article.paragraphs:
            pv = self.get_paragraph_view(article.meta.uuid, para)
            pv.hover_enabled = not editor_preview
            sc_cc.add_widget(pv)

    def get_paragraph_view(self, uuid, para: api.ArticlePara):
        rt = ArticleEntry()
        rt.article_uuid = uuid
        rt.article_para = 'para#' + str(para.order)

        vp = rt.ids.lbl
        del_img = True
        if para.p_type == tb_texting.TYPE_HEADER:
            if para.text.startswith('###'):
                vp.text = para.text[3:].strip()
                vp.font_size = 20
            elif para.text.startswith('##'):
                vp.text = para.text[2:].strip()
                vp.font_size = 22
            elif para.text.startswith('#'):
                vp.text = para.text[1:].strip()
                vp.font_size = 24

            vp.bold = True
        elif para.p_type == tb_texting.TYPE_CODE:
            vp.font_name = 'RobotoMono'
            vp.text = para.text
            vp.font_size = 14
        elif para.p_type == tb_texting.TYPE_IMAGE:
            del_img = False
            rt.ids.img.source = tb_texting.url_from_md_href(para.text)
        else:
            vp.text = tb_texting.wrap_refs(tb_texting.to_bbcode(para.text))

        if del_img:
            rt.remove_widget(rt.ids.img)
        else:
            rt.remove_widget(rt.ids.lbl)

        rt.ids.act.canvas.opacity = hover_opacity[0]
        a_rt = node_search_name(rt, 'actions')
        a_rt.remove_widget(node_search_name(a_rt, 'btn_react'))

        return rt

    def get_tags_view(self, meta: api.ArticleMeta):
        meta_row = ArticleEntry()
        meta_row.article_uuid = meta.uuid
        meta_row.article_para = 'tags'
        meta_row.remove_widget(meta_row.ids.img)

        vp = meta_row.ids.lbl
        vp.text = "Тэги: "

        if len(meta.tags) == 0:
            vp.text += "немае"
        else:
            for tag in meta.tags:
                vp.text += tb_texting.wrap_refs(f"[ref=tb://filter/by_tag/{tag}]{tag}[/ref], ")
            vp.text = vp.text[:-2]

        meta_row.ids.act.canvas.opacity = hover_opacity[0]
        a_rt = node_search_name(meta_row, 'actions')
        a_rt.remove_widget(node_search_name(a_rt, 'btn_comment'))
        a_rt.remove_widget(node_search_name(a_rt, 'btn_react'))

        return meta_row

    def get_changelog_view(self, meta: api.ArticleMeta):
        meta_row = ArticleEntry()
        meta_row.article_uuid = meta.uuid
        meta_row.article_para = 'meta'
        meta_row.remove_widget(meta_row.ids.img)

        vp = meta_row.ids.lbl
        vp.text = "Metadata changelog:"

        tsp = datetime.datetime.fromtimestamp(meta.origin_time / 1000).strftime(time_format_date)
        tsc = datetime.datetime.fromtimestamp(meta.create_time / 1000).strftime(time_format_date)
        tse = datetime.datetime.fromtimestamp(meta.edit_time / 1000).strftime(time_format_date)

        if meta.origin_url or meta.origin_time != 0:
            if meta.origin_time == 0:
                vp.text += "\n    "
            else:
                vp.text += f"\n    [b]{tsp}[/b] "

            vp.text += "originally posted"

            if meta.origin_url:
                hostname = urlparse(meta.origin_url).hostname
                if hostname is None:
                    vp.text += f" at \"{meta.origin_url}\""
                else:
                    vp.text += f" at [ref={meta.origin_url}]{hostname}[/ref]"

        cbb = meta.created_by
        cbd = api.uname(meta.created_by)
        vp.text += f"\n    [b]{tsc}[/b] brought to TB by [ref=tb://user/{cbb}]{cbd}[/ref]"

        if meta.create_time != meta.edit_time:
            abb = meta.edit_by
            abd = api.uname(meta.edit_by)
            vp.text += f"\n    [b]{tse}[/b] last edited by [ref=tb://user/{abb}]{abd}[/ref]"

        vp.text = tb_texting.wrap_refs(vp.text)

        meta_row.ids.act.canvas.opacity = hover_opacity[0]

        return meta_row

    def view_react_dialog(self):
        content = Builder.load_file('kv/popup_react.kv')

        popup = Popup(title='И каков твой вердикт?\nТекущая реакция: ' + self.get_text_my_reaction(),
                      title_color=[0, 0, 0, 1], content=content, auto_dismiss=False,
                      size_hint=(None, None), size=(dp(400), dp(400)))

        def react(button_reference):
            react_code = 0

            if button_reference.name == 'btn_bad':
                react_code = -1
            elif button_reference.name == 'btn_neutral':
                react_code = 0
            elif button_reference.name == 'btn_acceptable':
                react_code = 1
            elif button_reference.name == 'btn_gold':
                react_code = 2

            if api.can_db():
                api.rate_article(self.current_article, react_code)
            else:
                app().popup_cannot_db()

            popup.dismiss()

        content.ids.btn_bad.bind(on_press=react)
        content.ids.btn_neutral.bind(on_press=react)
        content.ids.btn_acceptable.bind(on_press=react)
        content.ids.btn_gold.bind(on_press=react)
        content.ids.btn_cancel.bind(on_press=popup.dismiss)

        popup.open()

    def get_text_my_reaction(self):
        react = api.get_my_reaction(self.current_article)

        if react == -2:
            return 'не голосовал'
        elif react == -1:
            return "ненадо так"
        elif react == 0:
            return 'воздержался'
        elif react == 1:
            return 'приемлемо'
        elif react == 2:
            return 'голд'

        return 'неизвестная реакция'

    def update_para(self, uuid: str, para: api.ArticlePara):
        sc_cc = node_search_name(self, 'scroll_content')
        sc_cc.children[para.order] = self.get_paragraph_view(uuid, para)


class ViewCommentsScreen(MScreen):
    article_id = ''
    comment_type = ''
    para_num = ''

    def view_comments(self, article: str, comm_type: str, num=0):
        app().set_title('Комменты')

        self.article_id = article
        self.comment_type = comm_type
        self.para_num = num

        sc_cc = node_search_name(self, 'scroll_content')
        sc_cc.clear_widgets()

        for comment in api.get_comments(article, None if comm_type == 'meta' else num):
            sc_cc.add_widget(self.get_comment(comment))

        sc_cc.add_widget(self.get_post_form())

    def get_post_form(self):
        root = Builder.load_file("kv/fragment_comment_field.kv")

        def postme(unused):

            if api.can_db():
                if self.comment_type == 'meta':
                    c = api.post_comment(self.article_id, None, root.ids.lbl.text)
                else:
                    c = api.post_comment(self.article_id, self.para_num, root.ids.lbl.text)

                sc_cc = node_search_name(self, 'scroll_content')
                sc_cc.add_widget(self.get_comment(c), index=1)

                root.ids.lbl.text = ''
            else:
                app().popup_cannot_db()

        root.ids.btn_post.bind(on_press=postme)

        return root

    def get_comment(self, comment: api.Comment):
        root = Builder.load_file("kv/fragment_comment_item.kv")

        time = datetime.datetime.fromtimestamp(comment.edit_time / 1000).strftime(time_format_date)
        root.text = f"[i]{time} by {comment.author_name}[i]\n{comment.comment}"

        return root


if __name__ == '__main__':
    api.create()

    Window.minimum_height = 400
    Window.minimum_width = 200
    Window.clearcolor = bgcolor
    Window.bind(on_resize=calc_responsibility)

    # Config.set('kivy', 'exit_on_escape', '0')
    if not is_droid():
        Config.set('input', 'mouse', 'mouse,multitouch_on_demand')

    LabelBase.register(name='RobotoMono', fn_regular='fonts/RobotoMono-VariableFont_wght.ttf')
    Factory.register('HoverBehavior', HoverBehavior)

    Builder.load_file("kv/style.kv")
    Builder.load_file("kv/style_list_items.kv")

    datapp = TreasureBox()

    if is_droid():
        import bugs
        bugs.fixBugs()

    datapp.run()

    api.destroy()
