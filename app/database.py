import os
import sqlite3

CURRENT_DB_VER = 1


class DB:
    ver: int
    conn: sqlite3.Connection or None

    def open(self):
        current_folder = os.path.abspath(os.path.dirname(__file__))
        self.conn = sqlite3.connect(os.path.join(current_folder, 'treasure-box.db'))
        self.ver = int(self.get_meta('version', 0))

        if self.ver < 1:
            self.upgrade_to_1()

        if self.ver != CURRENT_DB_VER:
            self.set_meta('version', CURRENT_DB_VER)

    def upgrade_to_1(self):
        self.conn.cursor().execute("""CREATE TABLE meta (
                key varchar(20)     NOT NULL UNIQUE,
                value varchar(20)   NOT NULL,
                PRIMARY KEY(key)
            )""")
        self.conn.cursor().execute("""CREATE TABLE article_meta (
                uuid char(36)           NOT NULL,
                edit_time datetime      NOT NULL DEFAULT CURRENT_TIMESTAMP,
                edited_by char(36)      NOT NULL,
                title text              NOT NULL,
                origin_uri text,
                published_time datetime,
                PRIMARY KEY(uuid, edit_time),
                FOREIGN KEY(edited_by)  REFERENCES accounts(uuid)
            )""")
        self.conn.cursor().execute("""CREATE TABLE article_text (
                edit_time datetime      NOT NULL DEFAULT CURRENT_TIMESTAMP,
                article char(36)        NOT NULL,
                edited_by char(36)      NOT NULL,
                para_order int          NOT NULL,
                para_type int           NOT NULL DEFAULT 0,
                para_text text,
                PRIMARY KEY(article, para_order, edit_time),
                FOREIGN KEY(article)    REFERENCES article_meta(uuid),
                FOREIGN KEY(edited_by)  REFERENCES accounts(uuid)
            )""")
        self.conn.cursor().execute("""CREATE TABLE accounts (
                uuid char(36)           NOT NULL UNIQUE,
                username text           NOT NULL,
                created_at datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY(uuid)
            )""")
        self.conn.cursor().execute("""CREATE TABLE tags (
                name text           NOT NULL UNIQUE,
                parent text,
                desc text,
                PRIMARY KEY(name),
                FOREIGN KEY(parent) REFERENCES tags(name)
            )""")
        self.conn.cursor().execute("""CREATE TABLE article_tags (
                article char(36)        NOT NULL,
                tag text                NOT NULL,
                edit_time datetime      NOT NULL DEFAULT CURRENT_TIMESTAMP,
                edited_by char(36)      NOT NULL,
                excluded boolean        DEFAULT 0,
                PRIMARY KEY(article, tag, edit_time),
                FOREIGN KEY(article)    REFERENCES article_meta(uuid),
                FOREIGN KEY(tag)        REFERENCES tags(name)
            )""")
        self.conn.cursor().execute("""CREATE TABLE comments (
                uuid char(36)           NOT NULL,
                edit_time datetime      NOT NULL DEFAULT CURRENT_TIMESTAMP,
                author char(36)         NOT NULL,
                article char(36)        NOT NULL,
                para int,
                comment text            NOT NULL,
                PRIMARY KEY(uuid, edit_time),
                FOREIGN KEY(author)     REFERENCES accounts(uuid),
                FOREIGN KEY(article)    REFERENCES article_meta(uuid),
                FOREIGN KEY(para)       REFERENCES article_text(para_order)
            )""")
        self.conn.cursor().execute("""CREATE TABLE reactions (
                article char(36)        NOT NULL,
                person char(36)         NOT NULL,
                type int                NOT NULL,
                time datetime           NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY(article, person),
                FOREIGN KEY(article)    REFERENCES article_meta(uuid),
                FOREIGN KEY(person)     REFERENCES accounts(uuid)
            )""")
        print("Database has been upgraded to v1")

    def close(self):
        self.conn.close()
        self.conn = None

    def is_locked(self):
        try:
            self.set_meta('lock_test', '4221')
            return False
        except sqlite3.OperationalError:
            return True

    def get_meta(self, key, default=None):
        try:
            return self.conn.cursor().execute("SELECT value FROM meta WHERE key = ?", [key]).fetchone()[0]
        except TypeError:
            return default
        except sqlite3.OperationalError:
            return default

    def set_meta(self, key, value):
        self.conn.cursor().execute("replace into meta(key, value) values (?,?)", (key, value))
        self.conn.commit()

    # Actual methods

    def insert_account(self, uuid: str, username):
        if not uuid.strip():
            raise RuntimeError('Dude we got empty UUID')

        self.conn.cursor().execute("insert into accounts(uuid, username) values (?,?)", (uuid, username))
        self.conn.commit()

    def insert_meta(self, uuid, time, user, title, origin='', published=0):
        if not uuid.strip():
            raise RuntimeError('Dude we got empty UUID')

        self.conn.cursor().execute("insert into article_meta(uuid, edit_time, edited_by, title,"
                                   " origin_uri, published_time)"
                                   " values (?,?,?,?,?,?)", (uuid, time, user, title, origin, published))
        self.conn.commit()

    def insert_text(self, time, article, user, order, para_type, text):
        if not article.strip():
            raise RuntimeError('Dude we got empty article UUID')

        self.conn.cursor().execute("insert into article_text(edit_time, article, edited_by,"
                                   " para_order, para_type, para_text)"
                                   " values (?,?,?,?,?,?)",
                                   (time, article, user, order, para_type, text))
        self.conn.commit()

    def insert_tag(self, tag):
        self.conn.cursor().execute("INSERT OR IGNORE INTO tags(name) VALUES(?);", [tag])
        self.conn.commit()

    def link_article_tag(self, article, tag, edit_time, edit_by, excluded=False):
        self.conn.cursor().execute("""
            INSERT INTO article_tags(article, tag, edit_time, edited_by, excluded) 
            VALUES(?,?,?,?,?)
        """, [article, tag, edit_time, edit_by, excluded])
        self.conn.commit()

    def get_article_tags(self, article):
        return self.conn.cursor().execute("""
            SELECT tag
            FROM article_tags
            INNER JOIN (
                SELECT article, tag as in_tag, excluded, max(edit_time) as etime
                FROM article_tags
                GROUP BY article_tags.article, article_tags.tag
            ) AS inner ON article_tags.article=inner.article AND article_tags.tag=in_tag
                AND article_tags.edit_time=etime
            WHERE article_tags.article = ? AND inner.excluded = 0
            ORDER BY article_tags.tag ASC
            """, [article]).fetchall()

    def get_all_articles(self):
        return self.conn.cursor().execute("""
            SELECT article_meta.uuid, edit_time, edited_by, title, origin_uri, published_time
            FROM article_meta
            INNER JOIN (
                SELECT uuid, max(edit_time) as etime
                FROM article_meta
                GROUP BY uuid
            ) AS inner ON article_meta.uuid = inner.uuid AND article_meta.edit_time = etime
            ORDER BY article_meta.title ASC
            """, []).fetchall()

    def get_meta_changes(self):
        return self.conn.cursor().execute(
            """
            SELECT article_meta.uuid as art_id, article_meta.title, article_meta.edit_time,
             accounts.uuid, accounts.username, 
            (
                SELECT min(inner_am.edit_time) FROM article_meta as inner_am
                WHERE inner_am.uuid = article_meta.uuid GROUP BY inner_am.uuid
            )
            FROM article_meta
            LEFT JOIN accounts ON article_meta.edited_by=accounts.uuid
            ORDER BY article_meta.edit_time DESC
            LIMIT 255
            """
        ).fetchall()

    def get_para_changes(self):
        return self.conn.cursor().execute(
            """
            SELECT 
                article_text.edited_by, accounts.username,
                article_text.para_order, article_text.para_text,
                article_text.article, article_meta.title, 
                article_text.edit_time
            FROM article_text
            LEFT JOIN accounts ON article_text.edited_by = accounts.uuid
            LEFT JOIN article_meta ON article_text.article = article_meta.uuid
            LEFT JOIN (
                SELECT article, para_order, min(edit_time) as min_time
                FROM article_text
                GROUP BY article, para_order
            ) AS timings ON article_text.article = timings.article
                AND article_text.para_order = timings.para_order 
            WHERE article_text.edit_time > timings.min_time
            ORDER BY article_text.edit_time DESC
            LIMIT 255
            """
        ).fetchall()

    def get_comment_changes(self):
        return self.conn.cursor().execute(
            """
            SELECT 
                comments.author, accounts.username,
                comments.para, comments.comment,
                comments.article, article_meta.title, 
                comments.edit_time
            FROM comments
            LEFT JOIN accounts ON comments.author = accounts.uuid
            LEFT JOIN (
                SELECT uuid AS outer_uuid, MAX(edit_time) AS outer_time
                FROM article_meta
                GROUP BY uuid
            ) ON comments.article = outer_uuid
            LEFT JOIN article_meta ON outer_uuid = article_meta.uuid AND outer_time = article_meta.edit_time
            ORDER BY comments.edit_time DESC
            LIMIT 255
            """
        ).fetchall()

    def get_latest_meta(self, art_id: str):
        return self.conn.cursor().execute(
            """
            SELECT uuid, edit_time, edited_by, title, origin_uri, published_time, first_time, first_edited
            FROM article_meta
            INNER JOIN (
                SELECT edit_time as 'first_time', edited_by as 'first_edited', uuid as 'inner_uuid'
                FROM article_meta AS inner_am WHERE inner_am.uuid = ?
                ORDER BY inner_am.edit_time ASC LIMIT 1
            ) ON inner_uuid = article_meta.uuid
            WHERE uuid = ?
            ORDER BY article_meta.edit_time DESC
            LIMIT 1
            """, [art_id, art_id]
        ).fetchone()

    def get_latest_text(self, art_id: str):
        return self.conn.cursor().execute(
            """
            SELECT edit_time, article_text.article, edited_by, article_text.para_order, para_type, para_text
            FROM article_text
            INNER JOIN (
                SELECT article, para_order, max(edit_time) as etime 
                FROM article_text
                WHERE article = ?
                GROUP BY para_order
            ) AS inner ON article_text.para_order=inner.para_order AND article_text.edit_time=inner.etime
            WHERE article_text.article = ?
            ORDER BY article_text.para_order ASC
            """, [art_id, art_id]
        ).fetchall()

    def get_username(self, user_id: str):
        return self.conn.cursor().execute("""
        SELECT username FROM accounts WHERE uuid = ? LIMIT 1
        """, [user_id]).fetchone()

    def list_tags(self):
        return self.conn.cursor().execute("SELECT name, parent, desc FROM tags ORDER BY name ASC").fetchall()

    def insert_reaction(self, article: str, person: str, react_type: int, time: int):
        self.conn.cursor().execute("""
            INSERT OR REPLACE INTO reactions(article, person, type, time)
            VALUES (?, ?, ?, ?)
        """, [article, person, react_type, time])
        self.conn.commit()

    def get_reaction(self, article: str, person: str):
        return self.conn.cursor().execute("""
            SELECT type, time FROM reactions 
            WHERE article = ? AND person = ?
        """, [article, person]).fetchone()

    def insert_comment(self, uuid: str, edit_time: int, author: str, article: str, para, comment: str):
        self.conn.cursor().execute("""
        INSERT INTO comments (uuid, edit_time, author, article, para, comment)
        VALUES(?, ?, ?, ?, ?, ?)
        """, [uuid, edit_time, author, article, para, comment])
        self.conn.commit()

    def get_comments(self, article: str, para):
        return self.conn.cursor().execute(f"""
            SELECT comments.uuid, edit_time, author, accounts.username, comment
            FROM comments
            LEFT JOIN accounts ON comments.author = accounts.uuid
            WHERE article = ? AND para {"IS NULL" if para is None else "= ?"}
            ORDER BY edit_time ASC
        """, [article] if para is None else [article, para]).fetchall()

    def get_para_text(self, article: str, para):
        return self.conn.cursor().execute(f"""
            SELECT para_text, edit_time, edited_by, para_type, para_order
            FROM article_text
            WHERE article = ? AND para_order = ?
            ORDER BY edit_time DESC
            LIMIT 1
        """, [article, para]).fetchone()

    def get_articles_by_tag(self, tag: str):
        return self.conn.cursor().execute("""
            SELECT article_tags.article, m2.title FROM article_tags
            LEFT JOIN (
                SELECT article, tag, max(edit_time) AS et FROM article_tags
                GROUP BY article_tags.article, article_tags.tag
            ) AS a1 ON article_tags.article = a1.article AND article_tags.tag = a1.tag
            LEFT JOIN (
                SELECT article, tag, edit_time, excluded FROM article_tags
            ) AS a2 ON article_tags.article = a2.article AND article_tags.tag = a2.tag AND a1.et = a2.edit_time
            LEFT JOIN (
                SELECT uuid, max(edit_time) AS et FROM article_meta GROUP BY uuid
            ) AS m1 ON article_tags.article = m1.uuid
            LEFT JOIN (
                SELECT uuid, edit_time, title FROM article_meta
            ) AS m2 ON article_tags.article = m1.uuid AND m1.et = m2.edit_time
            WHERE article_tags.tag = ? AND a2.excluded = 0
            GROUP BY article_tags.article, article_tags.tag
        """, [tag]).fetchall()

    def get_articles_by_substr(self, substr: str):
        return self.conn.cursor().execute("""
            SELECT DISTINCT article_text.article, article_meta.title, article_time.et
            FROM article_text
            LEFT JOIN (
                SELECT uuid, max(edit_time) AS et FROM article_meta
                GROUP BY uuid
            ) AS article_time ON article_text.article = article_time.uuid
            LEFT JOIN article_meta ON article_meta.uuid = article_time.uuid
                AND article_meta.edit_time = article_time.et
            WHERE para_text LIKE ?
        """, ['%' + substr + '%']).fetchall()

    # Raw query

    def raw_query_accounts(self):
        return self.conn.cursor().execute("""
            SELECT uuid, username, created_at FROM accounts
        """).fetchall()

    def raw_query_tags(self):
        return self.conn.cursor().execute("""
            SELECT name, parent, desc FROM tags
        """).fetchall()

    def raw_query_meta(self):
        return self.conn.cursor().execute("""
            SELECT uuid, edit_time, edited_by, title, origin_uri, published_time FROM article_meta
        """).fetchall()

    def raw_query_text(self):
        return self.conn.cursor().execute("""
            SELECT edit_time, article, edited_by, para_order, para_type, para_text FROM article_text
        """).fetchall()

    def raw_query_reactions(self):
        return self.conn.cursor().execute("""
            SELECT article, person, type, time FROM reactions
        """).fetchall()

    def raw_query_comments(self):
        return self.conn.cursor().execute("""
            SELECT uuid, edit_time, author, article, para, comment FROM comments
        """).fetchall()

    def raw_query_art_tags(self):
        return self.conn.cursor().execute("""
            SELECT article, tag, edit_time, edited_by, excluded FROM article_tags
        """).fetchall()

    # Raw insert

    def raw_insert_accounts(self, values):
        self.conn.cursor().executemany("""
            INSERT OR IGNORE INTO accounts(uuid, username, created_at) VALUES (?,?,?) 
        """, values)

    def raw_insert_tags(self, values):
        self.conn.cursor().executemany("""
            INSERT OR IGNORE INTO tags(name, parent, desc) VALUES (?,?,?) 
        """, values)

    def raw_insert_meta(self, values):
        self.conn.cursor().executemany("""
            INSERT OR IGNORE INTO article_meta(uuid, edit_time, edited_by, title, origin_uri, published_time) 
            VALUES (?,?,?,?,?,?) 
        """, values)

    def raw_insert_text(self, values):
        self.conn.cursor().executemany("""
            INSERT OR IGNORE INTO article_text(edit_time, article, edited_by, para_order, para_type, para_text)
            VALUES (?,?,?,?,?,?) 
        """, values)

    def raw_insert_reactions(self, values):
        self.conn.cursor().executemany("""
            INSERT OR IGNORE INTO reactions(article, person, type, time) VALUES (?,?,?,?) 
        """, values)

    def raw_insert_comments(self, values):
        self.conn.cursor().executemany("""
            INSERT OR IGNORE INTO comments(uuid, edit_time, author, article, para, comment) VALUES (?,?,?,?,?,?) 
        """, values)

    def raw_insert_art_tags(self, values):
        self.conn.cursor().executemany("""
            INSERT OR IGNORE INTO article_tags(article, tag, edit_time, edited_by, excluded) VALUES (?,?,?,?,?) 
        """, values)
