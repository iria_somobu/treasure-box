#!/bin/bash

export JAVA_HOME='/opt/jre1.8_92'

~/.local/bin/p4a apk \
  --sdk-dir /opt/android-sdk \
  --ndk-dir /opt/android-sdk/ndk/21.3.6528147 \
  --android-api '27' \
  --private ${PWD} \
  --package 'com.somobu.tb' \
  --name "Treasure Box" \
  --version 1.0 \
  --bootstrap=sdl2 \
  --requirements=python3,kivy,sqlite3
