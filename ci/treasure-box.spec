# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['../app/treasure-box.py'],
    binaries=[],
    datas=[
        ('../app/fonts',    'fonts/'),
        ('../app/img',      'img/'),
        ('../app/kv',       'kv/'),
    ],
    hiddenimports=[],
    hookspath=[],
    runtime_hooks=[],
    excludes=[ 
        # This was found on SO
        'wx', 'gtk+', 'Tkinter', 'gtk', 'gdk', 'gtk2', 'gtk3', 'cairo', 'wayland', 'xinerama', 
        'atk', 'pango', 'pil', 'PIL',
        'bsddb', 'curses', 'pywin.debugger', 'pywin.debugger.dbgcon', 'pywin.dialogs', 

        # This I added on my own
        'certifi', 'markdown', 'difflib', 'distutils', 'docutils', 'pygments', 'pkg_resources',
        '_tkinter', '_gtkagg', '_tkagg', 'Tkconstants', 'Tkinter', 'tcl',
        'gi', 'gi.repository.Gtk', 'gi.repository.Gdk', 'gi.repository.Gst', 'gi.repository.GObject'
         ],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False)

pyz = PYZ(a.pure, a.zipped_data,cipher=block_cipher)
exe = EXE(pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='treasure-box', debug=False, bootloader_ignore_signals=False, 
    strip=False, upx=True, console=False)
coll = COLLECT(exe, a.binaries, a.zipfiles, a.datas, strip=False, upx=True, upx_exclude=[], name='treasure-box')
