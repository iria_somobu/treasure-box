export JAVA_HOME=/opt/jre1.8_92

p4a apk --private ../app/ \
  --package=com.somobu.treasure_box \
  --name=TreasureBox \
  --version=0.1 \
  --bootstrap=sdl2 \
  --requirements=python3,kivy \
  --sdk-dir /opt/android-sdk \
  --blacklist=p4a-blacklist.txt

