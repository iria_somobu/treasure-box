#!/usr/bin/python

import os
import sqlite3

current_folder = os.path.abspath(os.path.dirname(__file__))
conn = sqlite3.connect(os.path.join(current_folder, '../app/treasure-box.db'))

rows = conn.cursor().execute("""
  SELECT uuid, title FROM article_meta
""").fetchall();

for row in rows:
  print(row[0] + ' ' + row[1]);

conn.close()

