# Fetch
target_class = 'post'


# Parse
def wrap_custom(node):
    if node.name == 'a':
        if 'wp-content/uploads' in node.attrs.get('href'):  # Ignore image links
            return walk_default(node)   # Ask host script to parse content
    elif node.name == 'div':
        # Ignore a few technical divs
        if 'entry-meta' in node.attrs.get('class'):
            return ''
        elif 'entry-utility' in node.attrs.get('class'):
            return ''

    elif node.name == 'figure':
        return walk_default(node)
    elif node.name == 'code':
        return f'`{walk_default(node)}`'

    return wrap_default(node)   # Fallback to default wrapper
