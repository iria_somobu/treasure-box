# Fetch
target_class = 'post_content'


# Parse
def wrap_custom(node):
    if node.name == 'h3': return f'## {walk_default(node)}'
    elif node.name == 'div':
        if node.attrs.get('class') == None: return wrap_default(node) 
        if 'mainheader' in node.attrs.get('class'): return "";
        if 'blog_results' in node.attrs.get('class'): return "";
    return wrap_default(node)   # Fallback to default wrapper
