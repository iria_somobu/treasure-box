#!/usr/bin/python3

import argparse
import bs4
import urllib.request
import os.path

from urllib.parse import urlparse


class bcolors: # Blender's bcolors
  HEADER = '\033[95m'
  OKBLUE = '\033[94m'
  OKCYAN = '\033[96m'
  OKGREEN = '\033[92m'
  WARNING = '\033[93m'
  FAIL = '\033[91m'
  ENDC = '\033[0m'
  BOLD = '\033[1m'
  UNDERLINE = '\033[4m'


# Step 0: parse commandline args
parser = argparse.ArgumentParser(description='TreasureBox fetcher')
parser.add_argument('source', nargs=1, help='source url')
args = parser.parse_args()

target_url = args.source[0]
domain = urlparse(target_url).hostname

target_type = 'div'
target_class = 'post_content'


# Additional step: extension loading
shorten_domain = domain
while '.' in shorten_domain:
  custom_script = f'tb_fetcher/{shorten_domain}.py'
  shorten_domain = shorten_domain[(shorten_domain.index('.') + 1):]
  if os.path.isfile(custom_script):
    print(f'{bcolors.OKBLUE}Found parser extension {custom_script}, loading it{bcolors.ENDC}')
    exec(open(custom_script).read())
  elif not '.' in shorten_domain:
    print(f'{bcolors.OKBLUE}No {custom_script} found, using defaults{bcolors.ENDC}')

output_filename = 'myfile'


# Step 1
print('Step 1: download file')

with urllib.request.urlopen(target_url) as fp:
  raw_html = fp.read().decode("utf8")


# Step 2
print('Step 2: parse html && pretty-print to file')

parsed_html = bs4.BeautifulSoup(raw_html, features="lxml")
target_node = parsed_html.body.find(target_type, attrs={'class':target_class})

with open(f"{output_filename}.html", "w")  as f:
  f.write(str(target_node.prettify()))


# Step 3
print('Step 3: convert html -> md')


def wrap_default(child):
  contents = walk_default(child)

  subst = {
    'a': f"[{contents}]({child.attrs.get('href')})",
    'b': f"**{contents}**",
    'br': f'\n\n',
    'div': f"{contents}",
    'h1': f'\n\n## {contents}',
    'h2': f'\n\n### {contents}',
    'h3': f'\n\n#### {contents}',
    'i': f"*{contents}*",
    'img': f"![]({child.attrs.get('src')})",
    'li': f'\n - {contents}',
    'ol': f'\n\n{contents}\n\n',
    'p': f'\n\n{contents}\n\n',
    'pre': f'\n```\n{contents}\n```',
    'span': f'{contents}',
    'strong': f"**{contents}**",
    'ul': f'\n\n{contents}\n\n',
  }

  if child.name in subst:
    return subst[child.name]
  else:
    print('\tWarning: unknown tag: <'+ child.name + '>')
    return walk_default(child)


def walk_default(node):
  result = ""
  for child in node.children:
    if isinstance(child, bs4.element.Comment):
      pass
    elif isinstance(child, bs4.element.NavigableString):
      result += str(child)
    else:
      try:
        result += wrap_custom(child)
      except NameError:
        result += wrap_default(child)

  return result


parsed_md = walk_default(target_node)


# Additional formatting and output
while '\n\n\n' in parsed_md:
  parsed_md = parsed_md.replace('\n\n\n', '\n\n')

with open(f"{output_filename}.md", "w") as f:
  f.write(parsed_md)


# Done
print(f'{bcolors.BOLD}{bcolors.OKGREEN}Done!{bcolors.ENDC}')

